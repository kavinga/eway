<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Controller\Api;

/**
 * Class Placement
 *
 * @package Netstarter\Eway\Controller\Api
 */
class Placement extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Netstarter\Eway\Model\Api\Redirect
     */
    protected $api;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Netstarter\Eway\Model\Api\Redirect $api
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->api             = $api;
    }

    public function execute()
    {
        $this->saveCheckoutSession();
        $order = $this->checkoutSession->getLastRealOrder();
        if ($order->getId()) {
            // Build gateway URL and formFields string
            $redirectFields = $this->api->getRedirectFields($order);
            $query          = http_build_query($redirectFields, '', '&');
            $query          = str_replace(' ', '%21', $query);

            // Send request, receive response
            $type     = \Netstarter\Eway\Model\Config::TYPE_REDIRECT;
            $gateway  = $this->api->getGatewayUrl($type, false, 'request');
            $request  = $this->api->curlPostXml($gateway . '?' . $query);
            $response = new \SimpleXMLElement($request);

            // Debug
            if ($this->api->getConfigData('debug_flag')) {
                $url           = $this->getRequest()->getPathInfo();
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $objectManager->create('\Netstarter\Eway\Model\Debug')
                    ->setDir('out')
                    ->setUrl($url)
                    ->setData('data', print_r($redirectFields, true))
                    ->save();
                $objectManager->create('\Netstarter\Eway\Model\Debug')
                    ->setDir('in')
                    ->setUrl($url)
                    ->setData('data', $request)
                    ->save();
            }

            // Switch response
            switch ($response->Result) {
                case \Netstarter\Eway\Model\Api\Redirect::STATUS_TRUE:
                    $this->_redirect($response->URI);
                    break;

                case \Netstarter\Eway\Model\Api\Redirect::STATUS_FALSE:
                    $errorMessage = (string)$response->Error;
                    $this->messageManager->addError($errorMessage);
                    $this->_redirect('checkout/cart', array('_secure' => true));
                    break;
                default:
            }
        } else {
            $this->_redirect('checkout/cart', array('_secure' => true));
        }
    }

    protected function saveCheckoutSession()
    {
        $this->checkoutSession->setEwayQuoteId($this->checkoutSession->getLastSuccessQuoteId());
        $this->checkoutSession->setEwayOrderId($this->checkoutSession->getLastOrderId(true));
    }
}
