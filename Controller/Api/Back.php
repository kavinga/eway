<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Controller\Api;

/**
 * Class Index
 *
 * @package Netstarter\Eway\Controller\Api
 */
class Back extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Netstarter\Eway\Model\Api
     */
    protected $api;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var \Netstarter\Eway\Model\Process
     */
    protected $process;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Netstarter\Eway\Model\Api\Redirect   $api
     * @param \Magento\Checkout\Model\Session       $checkoutSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Netstarter\Eway\Model\Api\Redirect $api,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Netstarter\Eway\Model\Process $process
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->api             = $api;
        $this->process         = $process;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (isset($params['AccessPaymentCode'])) {
            $resultFields = $this->api->getResultFields($params);
            $query        = http_build_query($resultFields, '', '&');
            $query        = str_replace(' ', '%21', $query);

            // Send request, receive response
            $type     = \Netstarter\Eway\Model\Config::TYPE_REDIRECT;
            $gateway  = $this->api->getGatewayUrl($type, false, 'result');
            $request  = $this->api->curlPostXml($gateway . '?' . $query);
            $response = new \SimpleXMLElement($request);


            // Debug
            if ($this->api->getConfigData('debug_flag')) {
                $url = $this->getRequest()->getPathInfo();
                $this->_objectManager->create('\Netstarter\Eway\Model\Debug')
                    ->setDir('out')
                    ->setUrl($url)
                    ->setData('data', print_r($resultFields, true))
                    ->save();
                $this->_objectManager->create('\Netstarter\Eway\Model\Debug')
                    ->setDir('in')
                    ->setUrl($url)
                    ->setData('data', $request)
                    ->save();
            }
            $order = $this->_objectManager->create('\Magento\Sales\Api\Data\OrderInterface')->loadByIncrementId((string)$response->MerchantInvoice);
            if (!$order->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
            }
            $note          = $this->api->getConfig()->getResponseMessage((string)$response->ResponseCode);
            $transactionId = (string)$response->AuthCode;

            // Switch response by status or code
            if (isset($response->TransactionStatus)) {
                switch ((string)$response->TransactionStatus) {
                    case 'true':
                        $this->process->success($order, $note, $transactionId, 1, true);
                        $this->_redirect('checkout/onepage/success', array('_secure' => true));
                        break;
                    default:
                        $this->process->cancel($order, $note, $transactionId, 1, true);
                        $this->_redirect('checkout/cart', array('_secure' => true));
                }
            } else {
                switch ((string)$response->ResponseCode) {
                    case '00':
                    case '08':
                    case '10':
                    case '11':
                    case '16':
                        $this->process->success($order, $note, $transactionId, 1, true);
                        $this->_redirect('checkout/onepage/success', array('_secure' => true));
                        break;

                    default:
                        $this->process->cancel($order, $note, $transactionId, 1, true);
                        $this->_redirect('checkout/cart', array('_secure' => true));
                }
            }
        } else {
            $this->process->repeat();
            $this->_redirect('checkout/cart', array('_secure' => true));
        }

    }

    /**
     * Instantiate quote and checkout
     *
     * @return Mage_Paypal_Model_Express_Checkout
     * @throws Mage_Core_Exception
     */
    protected function initCheckout()
    {
//        $quote = $this->checkoutSession->getQuote();
//        if (!$quote->hasItems() || $quote->getHasError()) {
//            $this->getResponse()->setHeader('HTTP/1.1', '403 Forbidden');
//            throw new \Magento\Framework\Exception\LocalizedException(__('Unable to initialize Express Checkout.'));
//        }

        return $this;
    }

    /**
     * Save checkout session
     */
    public function saveCheckoutSession()
    {
        $this->checkoutSession->setEwayQuoteId($this->checkoutSession->getLastSuccessQuoteId());
        $this->checkoutSession->setEwayOrderId($this->checkoutSession->getLastOrderId(true));
    }
}
