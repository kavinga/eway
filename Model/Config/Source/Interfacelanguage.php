<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Config\Source;

/**
 * Class Interfacelanguage
 *
 * @package Netstarter\Eway\Model\Config\Source
 */
class Interfacelanguage
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => \Netstarter\Eway\Model\Config::LANGUAGE_EN,
                'label' => __('English'),
            ),
            array(
                'value' => \Netstarter\Eway\Model\Config::LANGUAGE_ES,
                'label' => __('Spanish'),
            ),
            array(
                'value' => \Netstarter\Eway\Model\Config::LANGUAGE_FR,
                'label' => __('French'),
            ),
            array(
                'value' => \Netstarter\Eway\Model\Config::LANGUAGE_DE,
                'label' => __('German'),
            ),
            array(
                'value' => \Netstarter\Eway\Model\Config::LANGUAGE_NL,
                'label' => __('Dutch'),
            ),
        );
    }

}
