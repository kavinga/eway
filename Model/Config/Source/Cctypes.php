<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Config\Source;


class Cctypes extends \Magento\Payment\Model\Config\Source\Cctype
{

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        /** @var array $cardTypesAllowed card types allowed by eway gw */
        $cardTypesAllowed = ['AE', 'VI', 'MC', 'JCB', 'EWAY_MAESTROUK', 'EWAY_DINERSCLUB'];
        $options          = [];

        foreach ($this->_paymentConfig->getCcTypes() as $code => $name) {
            if (in_array($code, $cardTypesAllowed)) {
                $options[] = ['value' => $code, 'label' => $name];
            }
        }

        return $options;
    }

} 