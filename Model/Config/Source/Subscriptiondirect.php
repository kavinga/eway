<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Config\Source;

/**
 * Class Subscriptiondirect
 *
 * @package Netstarter\Eway\Model\Config\Source
 */
class Subscriptiondirect
{
    /**
     * Subscriptions List
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array(
                'value' => \Netstarter\Eway\Model\Config::COUNTRY_AU,
                'label' => __('eWAY Australia'),
            ),
            array(
                'value' => \Netstarter\Eway\Model\Config::COUNTRY_NZ,
                'label' => __('eWAY New Zealand'),
            ),
        );
    }
}