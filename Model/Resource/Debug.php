<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Resource;

/**
 * Class Debug
 *
 * @package Netstarter\Eway\Model\Resource
 */
class Debug extends \Magento\Framework\Model\Resource\Db\AbstractDb
{

    /**
     * Init Resource model and connection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eway_api_debug', 'debug_id');
    }
}
