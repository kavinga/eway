<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model;

/**
 * Class Cc
 *
 * @package Netstarter\Eway\Model\Method
 */
class Cc extends \Magento\Payment\Model\Method\Cc
{
    /**
     * response code status success
     */
    const STATUS_TRUE = 'True';
    /**
     * response code status failure
     */
    const STATUS_FALSE = 'False';

    /**
     * {@inheritdoc}
     */
    protected $_code = 'eway_rapid';

    /**
     * {@inheritdoc}
     */
    protected $_isGateway = true;
    /**
     * {@inheritdoc}
     */
    protected $_canOrder = false;
    /**
     * {@inheritdoc}
     */
    protected $_canAuthorize = false;
    /**
     * {@inheritdoc}
     */
    protected $_canCapture = true;
    /**
     * {@inheritdoc}
     */
    protected $_canCapturePartial = true;
    /**
     * {@inheritdoc}
     */
    protected $_canRefund = true;
    /**
     * {@inheritdoc}
     */
    protected $_canRefundInvoicePartial = true;
    /**
     * {@inheritdoc}
     */
    protected $_canVoid = true;
    /**
     * {@inheritdoc}
     */
    protected $_canUseInternal = false;
    /**
     * {@inheritdoc}
     */
    protected $_canUseCheckout = true;
    /**
     * {@inheritdoc}
     */
    protected $_isInitializeNeeded = false;
    /**
     * {@inheritdoc}
     */
    protected $_canFetchTransactionInfo = false;
    /**
     * {@inheritdoc}
     */
    protected $_canReviewPayment = false;

    /**
     * {@inheritdoc}
     * Do not store cc data in magento
     */
    protected $_canSaveCc = false;

    /**
     * @var array $_allowCurrencyCode restrictions
     */
    protected $allowCurrencyCode = ['GBP', 'AUD', 'NZD', 'USD'];

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Eway config
     *
     * @var Config
     */
    protected $config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Helper
     *
     * @var \Netstarter\Eway\Helper\Data
     */
    protected $helper;

    /**
     * Encryptor
     *
     * @var \Magento\Framework\Encryption\EncryptorInterface
     */
    protected $encryptor;


    /**
     * @param \Magento\Framework\Model\Context                     $context
     * @param \Magento\Framework\Registry                          $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory    $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory         $customAttributeFactory
     * @param \Magento\Payment\Helper\Data                         $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface   $scopeConfig
     * @param \Magento\Framework\Module\ModuleListInterface        $moduleList
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Centinel\Model\Service                      $centinelService
     * @param \Magento\Store\Model\StoreManagerInterface           $storeManager
     * @param Config                                               $config
     * @param \Magento\Framework\Model\Resource\AbstractResource   $resource
     * @param \Magento\Framework\Data\Collection\Db                $resourceCollection
     * @param array                                                $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Centinel\Model\Service $centinelService,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Config $config,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Netstarter\Eway\Helper\Data $helper,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Framework\Model\Resource\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\Db $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $moduleList,
            $localeDate,
            $centinelService,
            $resource,
            $resourceCollection,
            $data
        );
        $this->storeManager    = $storeManager;
        $this->config          = $config;
        $this->checkoutSession = $checkoutSession;
        $this->helper          = $helper;
        $this->encryptor       = $encryptor;
    }

    /**
     * Validate if payment is possible
     *  - check allowed currency codes
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate()
    {
        parent::validate();
        $currency_code = $this->getCurrencyCode();
        if (!empty($this->allowCurrencyCode) && !in_array($currency_code, $this->allowCurrencyCode)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Selected currency (%1) is not compatible with this payment method.', $currency_code));
        }
        return $this;
    }

    /**
     * Decide currency code type
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->storeManager->getStore()->getBaseCurrencyCode();
    }

    /**
     * Create XML Message
     *
     * @param $fields array
     *
     * @return string
     */
    public function createXmlMessage($fields)
    {
        $xmlMessage = '';
        $xmlMessage .= '<?xml version="1.0" ?>' . "\n";
        $xmlMessage .= '<ewaygateway>' . "\n";
        foreach ($fields as $key => $value) {
            $xmlMessage .= '<' . $key . '>' . $value . '</' . $key . '>' . "\n";
        }
        $xmlMessage .= '</ewaygateway>' . "\n";
        return $xmlMessage;
    }


    /**
     * Post with CURL and return response
     *
     * @param $postUrl The URL with ?key=value
     * @param $postXml string XML message
     *
     * @return reponse XML Object
     */
    public function curlPostXml($postUrl, $postXML = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($postXML) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/xml']);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$postXML");
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }


    /**
     * Get gateway Url
     *
     * @param  string $type
     * @param bool    $security
     * @param bool    $mode
     *
     * @return mixed
     */
    public function getGatewayUrl($type, $security = false, $mode = false)
    {
        $subscription = $this->getConfigData('subscription');
        $gateways     = $this->config->getGateways();
        $test         = $this->getConfigData('test_flag') ? 'test' : 'live';

        switch ($type) {
            case \Netstarter\Eway\Model\Config::TYPE_XML:
                $url = $gateways[$subscription][$type][$security][$test];
                break;

            case \Netstarter\Eway\Model\Config::TYPE_REDIRECT:
                $url = $gateways[$subscription][$type][$mode];
                break;

            case \Netstarter\Eway\Model\Config::TYPE_TOKEN:
            case \Netstarter\Eway\Model\Config::TYPE_REFUND:
                $url = $gateways[$subscription][$type][$test];
                break;

            case \Netstarter\Eway\Model\Config::TYPE_RAPID:
                $url = $gateways[$subscription][$type][$security];
                break;

            default:
        }
        return $url;
    }
}
