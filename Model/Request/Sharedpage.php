<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Request;

/**
 * Class Sharedpage
 *
 * @package Netstarter\Eway\Model\Request
 */
class Sharedpage extends \Magento\Framework\Object
{

    public function createAccessCode($returnUrl = null, $cancelUrl = null)
    {
        // Empty Varien_Object's data
        $this->unsetData();
        $token       = null;
        $paypal      = null;
        $totalAmount = 0;

        if ($this->getMethod() == Eway_Rapid31_Model_Config::PAYMENT_SAVED_METHOD) {
            if ($this->_isNewToken()) {
                $returnUrl .= '?newToken=1';
                $method = Eway_Rapid31_Model_Config::METHOD_CREATE_TOKEN;
            } elseif ($token = $this->_editToken()) {
                $returnUrl .= '?editToken=' . $token;
                $token  = Mage::helper('ewayrapid/customer')->getCustomerTokenId($token);
                $method = Eway_Rapid31_Model_Config::METHOD_UPDATE_TOKEN;
            }
        } else {
            if (Mage::helper('ewayrapid')->getPaymentAction() === Mage_Payment_Model_Method_Abstract::ACTION_AUTHORIZE_CAPTURE) {
                $method = Eway_Rapid31_Model_Config::METHOD_PROCESS_PAYMENT;
            } else {
                $method = Eway_Rapid31_Model_Config::METHOD_AUTHORISE;
            }
            $totalAmount = round($this->_quote->getBaseGrandTotal() * 100);
            $paypal      = $this->_getPaypalCheckout();
            if ($paypal === Eway_Rapid31_Model_Config::PAYPAL_EXPRESS_METHOD) {
                $this->setCheckoutPayment(true);
                $this->setCheckoutURL(Mage::getUrl('ewayrapid/sharedpage/review'));
            }
        }

        $this->_buildRequest();

        $customer = $this->getCustomer();
        $customer->setTokenCustomerID($token ? $token : '');
        $this->setCustomer($customer);

        // prepare API
        $this->setRedirectUrl($returnUrl);
        $this->setCancelUrl($cancelUrl);
        $this->setMethod($method);

        if (Mage::helper('ewayrapid')->getTransferCartLineItems()) {
            // add Shipping item and Line items
            $lineItems = Mage::helper('ewayrapid')->getLineItems();
            $this->setItems($lineItems);
        }

        // add Payment
        $paymentParam = Mage::getModel('ewayrapid/field_payment');
        $paymentParam->setTotalAmount($totalAmount);
        $paymentParam->setCurrencyCode($this->_quote->getBaseCurrencyCode());
        $this->setPayment($paymentParam);

        $response = $this->_doRapidAPI('AccessCodesShared');
        return $response;
    }
}
