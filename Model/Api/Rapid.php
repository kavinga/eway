<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Api;


class Rapid extends \Netstarter\Eway\Model\Cc
{
    const PAYMENT_METHOD_EWAY_CODE = 'eway_rapid';

    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD_EWAY_CODE;
    /**
     * Bank Transfer payment block paths
     *
     * @var string
     */
//    protected $_formBlockType = 'Magento\OfflinePayments\Block\Form\Purchaseorder';
    protected $_formBlockType = 'Netstarter\Eway\Block\Form\Rapid';

    // Magento features
    protected $_canAuthorize = false;
    protected $_canCapture = true;
    protected $_canRefund = true;
    protected $_canRefundPartial = true;
    protected $_canCreateBillingAgreement = true;
    protected $_canManageBillingAgreements = true;


    // Actions
    const ACTION_PROCESS_PAYMENT = 'ProcessPayment';

    // Response Modes
    const RESPONSE_MODE_REDIRECT = 'Redirect';
    const RESPONSE_MODE_RETURN   = 'Return';

    public function __construct2(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Centinel\Model\Service $centinelService,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Netstarter\Eway\Model\Config $config,
        \Magento\Framework\Model\Resource\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\Db $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $moduleList,
            $localeDate,
            $centinelService,
            $storeManager,
            $config,
            $resource,
            $resourceCollection,
            $data
        );
        $this->checkoutSession = $checkoutSession;
        $this->objectManager   = $objectManager;
    }


    public function capture(\Magento\Framework\Object $payment, $amount)
    {
        if (!$this->canCapture()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The capture action is not available.'));
        }
        if ($amount <= 0) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid amount for capture.'));
        }

        $payment->setAmount($amount);
        $order = $payment->getOrder();

        $accessFields = $this->getAccessFields($order);


        // Build SOAP request
        $soapClient = new \SoapClient($this->getRapidGatewayUrl('soap'), array(
            'trace'          => 1,
            'authentication' => SOAP_AUTHENTICATION_BASIC,
            'login'          => $this->encryptor->decrypt((string)$this->getConfigData('api_key')),
            'password'       => $this->encryptor->decrypt((string)$this->getConfigData('rapid_password')),
        ));

        // SOAP Response
        try {
            $response = $soapClient->CreateAccessCode(array('request' => $accessFields));
        } catch (\SoapFault $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->faultstring));
        }
        // If we have a valid Access Code, we do a Curl Post for the
        // Card Data we already collected
        $response = $response->CreateAccessCodeResult;
        if (isset($response->AccessCode) && !empty($response->AccessCode)) {
            $accessCode = (string)$response->AccessCode;

            // curlPost the Card Fields
            $cardFields  = $this->getCardFields($order, $accessCode);
            $queryString = http_build_query($cardFields, '', '&');
            $this->curlPost($response->FormActionURL, $queryString);
            $this->captureProcess($payment, $response->AccessCode);
        } else {
            $note = 'Failed to access eWAY. Please contact the merchant.' . ' ' . $this->buildNote($response, (string)$response->Errors);
            throw new \Magento\Framework\Exception\LocalizedException(__($note));
        }

        $payment->setSkipTransactionCreation(true);
        return $this;
    }


    /**
     * Refund specified amount for payment
     *
     * @param \Magento\Framework\Object $payment
     * @param float                     $amount
     *
     * @return $this
     */
    public function refund(\Magento\Framework\Object $payment, $amount)
    {
        if (!$this->canRefund()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Refund action is not available.'));
        }

        // Refund through eWAY
        if ($payment->getRefundTransactionId() && $amount > 0) {
            $order = $payment->getOrder();
            if ($order->getId()) {
                // Build gateway URL and $directFields XML message
                $refundFields = $this->getRefundFields($order);
                $xmlMessage   = $this->createXmlMessage($refundFields);

                // Send request, receive response
                $type     = \Netstarter\Eway\Model\Config::TYPE_REFUND;
                $gateway  = $this->getGatewayUrl($type);
                $request  = $this->curlPostXml($gateway, $xmlMessage);
                $response = new \SimpleXMLElement($request);

                // Debug
                if ($this->getConfigData('debug_flag')) {
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $objectManager->create('\Netstarter\Eway\Model\Debug')
                        ->setDir('out')
                        ->setUrl('')
                        ->setData('data', print_r($refundFields, true))
                        ->save();
                    $objectManager->create('\Netstarter\Eway\Model\Debug')
                        ->setDir('in')
                        ->setUrl('')
                        ->setData('data', $request)
                        ->save();
                }

                $transactionId = (string)$response->ewayTrxnNumber;
                $responseCode  = substr((string)$response->ewayTrxnError, 0, 2);
                $note          = $this->config->getResponseMessage($responseCode);

                // Switch response
                switch ((string)$response->ewayTrxnStatus) {
                    case self::STATUS_FALSE:
                        throw new \Magento\Framework\Exception\LocalizedException(__('eWAY refund failed.'));
                        break;

                    case self::STATUS_TRUE:
                    default:
                }
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(__('Invalid order for refund.'));
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid transaction for refund.'));
        }

        return $this;
    }


    public function curlPost($postUrl, $postData = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($postData) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$postData");
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
     * @param \Magento\Framework\Object $payment
     * @param                           $accessCode
     *
     * @return \Netstarter\Eway\Model\Api\Rapid
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function captureProcess(\Magento\Framework\Object $payment, $accessCode)
    {
        if ($accessCode) {
            $resultFields = $this->getResultFields($accessCode);

            // Build SOAP request
            $soapClient = new \SoapClient($this->getRapidGatewayUrl('soap'), array(
                'trace'          => 1,
                'authentication' => SOAP_AUTHENTICATION_BASIC,
                'login'          => $this->encryptor->decrypt((string)$this->getConfigData('api_key')),
                'password'       => $this->encryptor->decrypt((string)$this->getConfigData('rapid_password')),
            ));

            // SOAP Response
            try {
                $response = $soapClient->GetAccessCodeResult(array('request' => $resultFields));
            } catch (SoapFault $e) {
                throw new \Magento\Framework\Exception\LocalizedException($e->faultstring);
            }

            // Debug
            if ($this->getConfigData('debug_flag')) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $objectManager->create('Netstarter\Eway\Model\Debug')
                    ->setDir('out')
                    ->setUrl('checkout/onepage')
                    ->setData('data', print_r($resultFields, true))
                    ->save();
                $objectManager->create('Netstarter\Eway\Model\Debug')
                    ->setDir('in')
                    ->setUrl('checkout/onepage')
                    ->setData('data', print_r($response, true))
                    ->save();
            }

            // Process the final access response!
            $response             = $response->GetAccessCodeResultResult;
            $gatewayTransactionId = (string)$response->TransactionID;

            // Build response note for backend
            $fraud = 0;
            $note  = $this->buildNote($response, (string)$response->ResponseCode, $fraud);

            // Switch response
            switch ((string)$response->TransactionStatus) {
                case 1:
                    $this->addTransaction(
                        $payment,
                        $gatewayTransactionId,
                        \Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE,
                        array('is_transaction_closed' => 0),
                        array('Transaction ID' => $gatewayTransactionId),
                        $note
                    );
                    break;

                default:
                    throw new \Magento\Framework\Exception\LocalizedException('Please check your card details and try again.');
            }

            $payment->setLastTransId($gatewayTransactionId);
            return $this;
        }
    }

    public function getRapidGatewayUrl($type, $security = false, $mode = false)
    {
        $gateways = $this->config->getRapidGateways();
        $test     = $this->getConfigData('test_flag') ? 'sandbox' : 'live';

        switch ($type) {
            case 'soap':
            case 'rest':
                $url = $gateways[$test][$type];
                break;

            case 'http':
            case 'rpc':
                $url = $gateways[$test][$type][$security];
                break;

            default:
        }
        return $url;
    }

    /**
     * @param string $accessCode
     *
     * @return array access code
     */
    public function getResultFields($accessCode)
    {
        $resultFields               = array();
        $resultFields['AccessCode'] = $accessCode;
        return $resultFields;
    }

    /**
     * Generates array of fields for redirect form
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return array
     */
    public function getAccessFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $storeId         = $order->getStoreId();
        $billingAddress  = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();
        if (!$shippingAddress || !is_object($shippingAddress)) {
            $shippingAddress = $billingAddress;
        }
        $paymentMethodCode = $order->getPayment()->getMethod();

        $accessFields = array();

        // Basics
        $accessFields['RedirectUrl'] = $this->config->getRapidUrl('redirect', $storeId);
        $accessFields['Method']      = self::ACTION_PROCESS_PAYMENT;
        $accessFields['CustomerIP']  = $this->helper->getRealIpAddr();

        // Payment
        $accessFields['Payment']['TotalAmount']        = number_format($order->getBaseGrandTotal(), 2, '.', '') * 100;
        $accessFields['Payment']['InvoiceNumber']      = substr($order->getIncrementId(), 0, 64);
        $accessFields['Payment']['InvoiceDescription'] = substr($this->config->getOrderDescription($order), 0, 64);
        $accessFields['Payment']['InvoiceReference']   = substr($order->getIncrementId(), 0, 64);

        // Customer
        $accessFields['Customer']['Reference']   = substr($order->getCustomerId(), 0, 50);
        $accessFields['Customer']['Title']       = $this->config->getGenderCode($order->getCustomerGender());
        $accessFields['Customer']['FirstName']   = substr($billingAddress->getFirstname(), 0, 50);
        $accessFields['Customer']['LastName']    = substr($billingAddress->getLastname(), 0, 50);
        $accessFields['Customer']['CompanyName'] = substr($billingAddress->getCompany(), 0, 50);
        $accessFields['Customer']['Street1']     = substr(isset($billingAddress->getStreet()[0]) ? $billingAddress->getStreet()[0] : '', 0, 50);
        $accessFields['Customer']['Street2']     = substr(isset($billingAddress->getStreet()[1]) ? $billingAddress->getStreet()[1] : '', 0, 50);
        $accessFields['Customer']['City']        = substr($billingAddress->getCity(), 0, 50);
        $accessFields['Customer']['State']       = substr($billingAddress->getState(), 0, 50);
        $accessFields['Customer']['PostalCode']  = substr($billingAddress->getPostcode(), 0, 30);
        $accessFields['Customer']['Country']     = strtolower(substr($billingAddress->getCountry(), 0, 2));
        $accessFields['Customer']['Email']       = substr($billingAddress->getEmail(), 0, 50);

        // Phone & fax cause trouble if they are not valid,...
        // $accessFields['Customer']['Phone'] = substr($billingAddress->getTelephone(), 0, 50);
        // $accessFields['Customer']['Fax'] = substr($billingAddress->getFax(), 0, 50);

        // Line items,...
        // @todo implement line items (+shipping and +-discount)
        // $accessFields['Items']['LineItem']['SKU'] = '';
        // $accessFields['Items']['LineItem']['Description'] = '';
        // $accessFields['Items']['LineItem']['Quantity'] = '';
        // $accessFields['Items']['LineItem']['UnitCost'] = '';
        // $accessFields['Items']['LineItem']['Tax'] = '';
        // $accessFields['Items']['LineItem']['Total'] = '';

        // Shipping address
        // $accessFields['ShippingAddress']['ShippingMethod'] = '';
        $accessFields['ShippingAddress']['FirstName']  = substr($shippingAddress->getFirstname(), 0, 50);
        $accessFields['ShippingAddress']['LastName']   = substr($shippingAddress->getLastname(), 0, 50);
        $accessFields['ShippingAddress']['Street1']    = substr(isset($shippingAddress->getStreet()[0]) ? $shippingAddress->getStreet()[0] : '', 0, 50);
        $accessFields['ShippingAddress']['Street2']    = substr(isset($shippingAddress->getStreet()[1]) ? $shippingAddress->getStreet()[1] : '', 0, 50);
        $accessFields['ShippingAddress']['City']       = substr($shippingAddress->getCity(), 0, 50);
        $accessFields['ShippingAddress']['State']      = substr($shippingAddress->getState(), 0, 50);
        $accessFields['ShippingAddress']['PostalCode'] = substr($shippingAddress->getPostcode(), 0, 30);
        $accessFields['ShippingAddress']['Country']    = strtolower(substr($shippingAddress->getCountry(), 0, 2));
        $accessFields['ShippingAddress']['Email']      = substr($shippingAddress->getEmail(), 0, 50);

        // Phone & fax cause trouble if they are not valid,...
        // $accessFields['ShippingAddress']['Fax'] = substr($shippingAddress->getFax(), 0, 50);
        // $accessFields['ShippingAddress']['Phone'] = substr($shippingAddress->getTelephone(), 0, 50);

        // Custom Fields
        // $accessFields['Options']['OptionName']['Value'] = substr('', 0, 255);
        return $accessFields;
    }

    /**
     * Generates array of card fields for SOAP
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param string                                 $accessCode
     * @param bool                                   $response
     *
     * @return array
     */
    public function getCardFields(\Magento\Sales\Api\Data\OrderInterface $order, $accessCode, $response = false)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $cardFields                    = array();
        $cardFields['EWAY_ACCESSCODE'] = $accessCode;

        // Expiration date mm/yy
        $month = $order->getPayment()->getCcExpMonth();
        $mm    = (string)$month < 10 ? '0' . $month : $month;
        $yy    = substr((string)$order->getPayment()->getCcExpYear(), 2, 2);

        $cardFields['EWAY_CARDNAME']        = $order->getPayment()->getCcOwner();
        $cardFields['EWAY_CARDNUMBER']      = $order->getPayment()->getCcNumber();
        $cardFields['EWAY_CARDEXPIRYMONTH'] = $mm;
        $cardFields['EWAY_CARDEXPIRYYEAR']  = $yy;
        $cardFields['EWAY_CARDCVN']         = $order->getPayment()->getCcCid();

        return $cardFields;
    }

    /**
     * Build $note with error message
     *
     * @param string  $response
     * @param  string $codes
     * @param bool    $fraud
     *
     * @return string
     */
    public function buildNote($response, $codes, &$fraud = false)
    {
        $note          = '';
        $responseCodes = explode(',', $codes);

        // Main response message
        if (isset($response->ResponseMessage)) {
            $note .= __('Response Message: %1', (string)$response->ResponseMessage);
        }

        // Error messages
        foreach ($responseCodes as $code) {
            if (substr($code, 0, 1) == 'F') {
                ++$fraud;
            }
            $responseMessage = $this->config->getResponseMessage($code);
            $note .= '<br /> - ' . __('%1 (CODE: %2).', $responseMessage, $code);
        }

        // Beagle score
        if (isset($response->BeagleScore)) {
            $note .= '<br />' . __('Beagle Score: %1', (string)$response->BeagleScore);
        }

        return $note;
    }


    /**
     * Add payment transaction
     *
     * @param \Magento\Sales\Api\Data\OrderPaymentInterface $payment
     * @param                                               $transactionId
     * @param                                               $transactionType
     * @param array                                         $transactionDetails
     * @param array                                         $transactionAdditionalInfo
     * @param bool                                          $message
     *
     * @return string
     */
    protected function addTransaction(
        \Magento\Framework\Object $payment,
        $transactionId,
        $transactionType,
        $transactionDetails = [],
        $transactionAdditionalInfo = [],
        $message = false
    ) {
        $message = $message . '<br />';
        $payment->setTransactionId($transactionId);
        $payment->resetTransactionAdditionalInfo();
        foreach ($transactionDetails as $key => $value) {
            $payment->setData($key, $value);
        }
        foreach ($transactionAdditionalInfo as $key => $value) {
            $payment->setTransactionAdditionalInfo($key, $value);
        }
        $transaction = $payment->addTransaction($transactionType, null, false, $message);
        foreach ($transactionDetails as $key => $value) {
            $payment->unsetData($key);
        }
        $payment->unsLastTransId();

        /**
         * It for self using
         */
        $transaction->setMessage($message);

        return $transaction;
    }

    /**
     * Check refund availability
     *
     * @return bool
     */
    public function canRefund()
    {
        // UK does not support refunds
        if ($this->getConfigData('subscription') == \Netstarter\Eway\Model\Config::COUNTRY_UK) {
            $this->_canRefund = false;
        }
        return $this->_canRefund;
    }

    /**
     * Generates array of fields for redirect form
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return array
     */
    public function getRefundFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $storeId           = $order->getStoreId();
        $paymentMethodCode = $order->getPayment()->getMethod();

        $refundFields                           = array();
        $refundFields['ewayCustomerID']         = substr($this->getConfigData('customer_id', $storeId), 0, 8);
        $amount                                 = number_format($order->getBaseGrandTotal(), 2, '.', '');
        $refundFields['ewayTotalAmount']        = round($amount * 100);
        $refundFields['ewayCardExpiryMonth']    = substr($order->getPayment()->getCcExpMonth(), 0, 2);
        $refundFields['ewayCardExpiryYear']     = substr($order->getPayment()->getCcExpYear(), 2, 2);
        $refundFields['ewayOriginalTrxnNumber'] = substr($order->getPayment()->getLastTransId(), 0, 16);
        $refundFields['ewayOption1']            = '';
        $refundFields['ewayOption2']            = '';
        $refundFields['ewayOption3']            = '';
        $refundFields['ewayRefundPassword']     = substr($this->encryptor->decrypt($this->getConfigData('refund_password', $storeId)), 0, 20);
        $refundFields['ewayCustomerInvoiceRef'] = substr($order->getIncrementId(), 0, 50);

        return $refundFields;
    }
}
