<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model\Api;

/**
 * Class Redirect
 *
 * @package Netstarter\Eway\Model\Api
 */
class Redirect extends \Netstarter\Eway\Model\Api
{
    protected $_code = 'eway_redirect';
    protected $_isInitializeNeeded = true;
    protected $_canUseInternal = false;
    protected $_canUseForMultishipping = false;

    /**
     * Get redirect URL after placing order
     *
     * @return string
     */
//    public function getCheckoutRedirectUrl()
//    {
//        return $this->config->getApiUrl('start');
//    }

    /**
     * Get redirect URL after placing order
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return $this->config->getApiUrl('placement');
    }

    /**
     * Generates array of fields for redirect form
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     *
     * @return array
     */
    public function getRedirectFields(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        if (empty($order)) {
            if (!($order = $this->getOrder())) {
                return array();
            }
        }

        $storeId           = $order->getStoreId();
        $billingAddress    = $order->getBillingAddress();
        $paymentMethodCode = $order->getPayment()->getMethod();

        $redirectFields               = array();
        $redirectFields['CustomerID'] = $this->getConfigData('customer_id', $storeId);
        $redirectFields['UserName']   = $this->getConfigData('username', $storeId);
        // Redirect amounts have 2 digits, e.g. 12.99
        $redirectFields['Amount']                    = number_format($order->getBaseGrandTotal(), 2, '.', '');
        $redirectFields['Currency']                  = $this->getCurrencyCode();
        $redirectFields['ReturnURL']                 = $this->config->getApiUrl('back', $storeId);
        $redirectFields['CancelURL']                 = $this->config->getApiUrl('cancel', $storeId);
        $redirectFields['ModifiableCustomerDetails'] = $this->config->getModifiableDetails($this->getConfigData('modifiable_details'));
        $redirectFields['Language']                  = $this->getConfigData('interface_language', $storeId);

        // Template constants
        $redirectFields['PageTitle']       = $this->config->getServiceConfigData('page_title', $storeId);
        $redirectFields['PageDescription'] = $this->config->getServiceConfigData('page_description', $storeId);
        $redirectFields['PageFooter']      = $this->config->getServiceConfigData('page_footer', $storeId);
        $redirectFields['CompanyName']     = $this->config->getServiceConfigData('company_name', $storeId);
        $redirectFields['CompanyLogo']     = $this->config->getServiceConfigData('company_logo', $storeId);
        $redirectFields['Pagebanner']      = $this->config->getServiceConfigData('page_banner', $storeId);

        // Customer data
        $redirectFields['CustomerFirstName'] = $billingAddress->getFirstname();
        $redirectFields['CustomerLastName']  = $billingAddress->getLastname();
        $address                             = $billingAddress->getStreet(0);
        $redirectFields['CustomerAddress']   = implode(' ', $address);
        $redirectFields['CustomerCity']      = $billingAddress->getCity();
        $redirectFields['CustomerState']     = $billingAddress->getRegion();
        $redirectFields['CustomersPostCode'] = $billingAddress->getPostcode();
        $redirectFields['CustomerCountry']   = $billingAddress->getCountry();
        $redirectFields['CustomerPhone']     = $billingAddress->getTelephone();
        $redirectFields['CustomerEmail']     = $billingAddress->getEmail();

        // Invoice
        $redirectFields['InvoiceDescription'] = '';
        $redirectFields['MerchantReference']  = $order->getId();
        $redirectFields['MerchantInvoice']    = $order->getIncrementId();

        return $redirectFields;
    }

    /**
     * Get result fields
     *
     * @param $params array
     *
     * @return array
     */
    public function getResultFields($params)
    {
        $storeId                           = $this->storeManager->getStore()->getId();
        $resultFields                      = array();
        $resultFields['CustomerID']        = $this->getConfigData('customer_id', $storeId);
        $resultFields['UserName']          = $this->getConfigData('username', $storeId);
        $resultFields['AccessPaymentCode'] = $params['AccessPaymentCode'];
        return $resultFields;
    }

    public function getConfig()
    {
        return $this->config;
    }
}
