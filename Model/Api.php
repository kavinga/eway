<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model;

/**
 * Class Api
 *
 * @package Netstarter\Eway\Model
 */
class Api extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_formBlockType = 'Netstarter\Eway\Block\Form';
    protected $_infoBlockType = 'Netstarter\Eway\Block\Info';

    // Magento features
    protected $_isGateway = false;
    protected $_canOrder = false;
    protected $_canAuthorize = false;
    protected $_canCapture = false;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canRefundInvoicePartial = false;
    protected $_canVoid = false;
    protected $_canUseInternal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    protected $_isInitializeNeeded = true;
    protected $_canFetchTransactionInfo = false;
    protected $_canReviewPayment = false;
    protected $_canCreateBillingAgreement = false;
    protected $_canManageRecurringProfiles = false;

    // Restrictions
    protected $_allowCurrencyCode = ['GBP', 'AUD', 'NZD', 'USD',];

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    // Response codes
    const STATUS_TRUE  = 'True';
    const STATUS_FALSE = 'False';

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        Config $config,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Model\Resource\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\Db $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $resource,
            $resourceCollection,
            $data
        );
        $this->config       = $config;
        $this->storeManager = $storeManager;
    }

    /**
     * Decide currency code type
     *
     * @return string
     */
    public function getCurrencyCode()
    {
        return $this->storeManager->getStore()->getBaseCurrencyCode();
    }

    /**
     * Get gateway Url
     *
     * @param  string $type
     * @param bool    $security
     * @param bool    $mode
     *
     * @return mixed
     */
    public function getGatewayUrl($type, $security = false, $mode = false)
    {
        $subscription = $this->getConfigData('subscription');
        $gateways     = $this->config->getGateways();
        $test         = $this->getConfigData('test_flag') ? 'test' : 'live';

        switch ($type) {
            case \Netstarter\Eway\Model\Config::TYPE_XML:
                $url = $gateways[$subscription][$type][$security][$test];
                break;

            case \Netstarter\Eway\Model\Config::TYPE_REDIRECT:
                $url = $gateways[$subscription][$type][$mode];
                break;

            case \Netstarter\Eway\Model\Config::TYPE_TOKEN:
            case \Netstarter\Eway\Model\Config::TYPE_REFUND:
                $url = $gateways[$subscription][$type][$test];
                break;

            case \Netstarter\Eway\Model\Config::TYPE_RAPID:
                $url = $gateways[$subscription][$type][$security];
                break;

            default:
        }
        return $url;
    }

    /**
     * Post with CURL and return response
     *
     * @param $postUrl The URL with ?key=value
     * @param $postXml string XML message
     *
     * @return reponse XML Object
     */
    public function curlPostXml($postUrl, $postXML = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $postUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if ($postXML) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/xml']);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$postXML");
        }

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
