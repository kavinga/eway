<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Model;

/**
 * Class Process
 *
 * @package Netstarter\Eway\Model
 */
class Process
{

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var Api\Redirect
     */
    protected $api;

    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;

    /**
     * @var Magento\Sales\Model\Order\Email\Sender\InvoiceSender
     */
    protected $invoiceSender;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Netstarter\Eway\Model\Api\Redirect $api,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->api             = $api;
        $this->objectManager   = $objectManager;
        $this->messageManager  = $messageManager;
        $this->orderSender     = $orderSender;
        $this->invoiceSender   = $invoiceSender;
    }

    /**
     * Success process
     * [multi-method] [multi-service]
     *
     * Update succesful (paid) orders, send order email, create invoice
     * and send invoice email. Restore quote and clear cart.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param string                                 $note          backend order history
     * @param string                                 $transactionId Transaction id
     * @param int                                    $responseCode  response code
     * @param bool                                   $frontend
     */
    public function success(\Magento\Sales\Api\Data\OrderInterface $order, $note, $transactionId, $responseCode = 1, $frontend = false)
    {
        $this->check($order);
        if ($order->getId() && $responseCode != $order->getPayment()->getAppmerceResponseCode()) {
            $order->getPayment()->setAppmerceResponseCode($responseCode);
            $order->getPayment()->setTransactionId($transactionId);
            $order->getPayment()->setLastTransId($transactionId);

            // Multi-method API
            $paymentMethodCode = $order->getPayment()->getMethod();

            // Send order email
            if (!$order->getEmailSent() && $this->api->getConfig()->getPaymentConfigData($paymentMethodCode, 'order_email')) {
                $this->orderSender->send($order);
//                $order->sendNewOrderEmail()->setEmailSent(true);
            }

            // Set processing status
            $processingOrderStatus = $this->api->getConfig()->getProcessingStatus($paymentMethodCode);
            $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, $processingOrderStatus, $note, $notified = false);
            $order->save();

            // Create invoice
            if ($this->api->getConfig()->getPaymentConfigData($paymentMethodCode, 'invoice_create')) {
                $this->invoice($order);
            }
        }

        if ($frontend) {
            $this->restore();
            $this->clear();
        }
    }

    /**
     * Check order state
     *
     * If the order state (not status) is already one of:
     * canceled, closed, holded or completed,
     * then we do not update the order status anymore.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     */
    public function check(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        if ($order->getId()) {
            $state = $order->getState();
            switch ($state) {
                case \Magento\Sales\Model\Order::STATE_HOLDED:
                case \Magento\Sales\Model\Order::STATE_CANCELED:
                case \Magento\Sales\Model\Order::STATE_CLOSED:
                case \Magento\Sales\Model\Order::STATE_COMPLETE:
                    exit();
                    break;
                default:
            }
        }
    }

    /**
     *  Create automatic invoice
     * [multi-method] [multi-service]
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     */
    public function invoice(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $this->check($order);

        // Multi-method API
        $paymentMethodCode = $order->getPayment()->getMethod();

        if (!$order->hasInvoices() && $order->canInvoice()) {
            $invoice = $order->prepareInvoice();
            if ($invoice->getTotalQty() > 0) {
                $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
                $invoice->setTransactionId($order->getPayment()->getTransactionId());
                $invoice->register();
                $transactionSave = $this->objectManager->create('Magento\Framework\DB\Transaction')->addObject($invoice)
                    ->addObject($invoice->getOrder());

//                $transaction = Mage::getModel('core/resource_transaction')->addObject($invoice)->addObject($invoice->getOrder());
                $transactionSave->save();
                $invoice->setCommentText(__('Automatic invoice.'));

                // Send invoice email
                if (!$invoice->getEmailSent() && $this->api->getConfig()->getPaymentConfigData($paymentMethodCode, 'invoice_email')) {
                    $this->invoiceSender->send($invoice);
                }
                $invoice->save();
            }
        }
    }

    /**
     * Cancel process
     *
     * Update failed, cancelled, declined, rejected etc. orders. Cancel
     * the order and show user message. Restore quote.
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @param                                        $note
     * @param                                        $transactionId
     * @param int                                    $responseCode
     * @param bool                                   $frontend
     */
    public function cancel(\Magento\Sales\Api\Data\OrderInterface $order, $note, $transactionId, $responseCode = 1, $frontend = false)
    {
        $this->check($order);
        if ($order->getId() && $responseCode != $order->getPayment()->getAppmerceResponseCode()) {
            $order->getPayment()->setAppmerceResponseCode($responseCode);
            $order->getPayment()->setTransactionId($transactionId);
            $order->getPayment()->setLastTransId($transactionId);

            // Cancel order
            $order->addStatusToHistory($order->getStatus(), $note, $notified = true);
            $order->cancel()->save();
        }

        if ($frontend) {
            $this->repeat();
        }
    }

    /**
     * Restore process
     *
     * Restore checkout session and show payment failed message.
     */
    public function repeat()
    {
        $this->restore();
        $this->messageManager->addError(__('Payment failed. Please try again.'));
    }

    /**
     * Clear cart
     */
    public function clear()
    {
        $this->checkoutSession->getQuote()->setIsActive(false)->save();
    }


    /**
     * Restore checkout session
     */
    public function restore()
    {
        $this->checkoutSession->setQuoteId($this->checkoutSession->getEwayQuoteId(true));
        $this->checkoutSession->setLastOrderId($this->checkoutSession->getEwayOrderId(true));
    }
}
