<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Block;

/**
 * Class Info
 *
 * @package Netstarter\Eway\Block
 */
class Info extends \Magento\Payment\Block\Info
{
    protected $_template = 'Netstarter_Eway::info.phtml';
}
