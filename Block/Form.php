<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Block;

/**
 * Class Form
 *
 * @package Netstarter\Eway\Block
 */
class Form extends \Magento\Payment\Block\Form
{
    protected $_template = 'Netstarter_Eway::form.phtml';

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
