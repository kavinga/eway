<?php
/**
 * Netstarter Pty Ltd.
 *
 * @category    Netstarter
 * @author      Netstarter Team <contact@netstarter.com>
 * @copyright   Copyright (c) 2014 Netstarter Pty Ltd. (http://www.netstarter.com.au)
 */

namespace Netstarter\Eway\Block\Form;

/**
 * Class Rapid
 *
 * @package Netstarter\Eway\Block\Form
 */
class Rapid extends \Magento\Payment\Block\Form\Cc
{
    protected $_template = 'Netstarter_Eway::form/rapid.phtml';
}
